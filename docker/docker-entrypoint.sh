#!/bin/bash

#/usr/sbin/sshd -f /home/gpadmin/.sshd/sshd_config

source /usr/local/gpdb/greenplum_path.sh

if [ "$GP_NODE" == "coordinator" ]
then
    echo 'Node type='$GP_NODE
    /usr/sbin/sshd -f /home/gpadmin/.sshd/sshd_config

    COORDINATOR_DATA_DIRECTORY=/var/lib/gpdb/data/coordinator/gpseg-1
    export COORDINATOR_DATA_DIRECTORY

    HOSTS=$(echo $HOSTSEGMENT | tr ";" "\n")
    for HOST in $HOSTS
    do
      ssh -o StrictHostKeyChecking=no $HOST true
      echo $HOST >> hostlist
    done

    if [ ! -d "$COORDINATOR_DATA_DIRECTORY" ]; then
	mkdir -p /var/lib/gpdb/data/coordinator
        echo 'Master directory does not exist. Initializing master from gpinitsystem_reflect.'
        gpssh-exkeys -f hostlist
        echo "Key exchange complete"
        gpinitsystem -a  -c gpinitsystem_config --su_password=dataroad
	##gpinitsystem -a  -c gpinitsystem_config -s scdw --mirror-mode=spread --su_password=dataroad
        echo "Master node initialized"
        # receive connection from anywhere.. This should be changed!!
        echo "host all all 0.0.0.0/0 md5" >> /var/lib/gpdb/data/coordinator/gpseg-1/pg_hba.conf
        gpstop -u
    else
        echo 'Master exists. Restarting gpdb.'
        gpstart -a
    fi
else
    echo 'Node type='$GP_NODE
    mkdir -p /var/lib/gpdb/data/primary
    mkdir -p /var/lib/gpdb/data/mirror
fi



exec "$@"
