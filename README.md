# To start: 
```bash
docker-compose build
docker-compose up -d
```

# To check
```bash
docker-compose ps
docker-compose logs cdw
docker-compose exec cdw bash
>source /usr/local/gpdb/greenplum_path.sh
>psql
```

# To stop
```bash
docker-compose down --volumes
```
